import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15

Rectangle {
    id: root
    color: "#2E2E36"
    clip: true

    signal mousePressed()
    signal topEdgeMousePressed()
    signal doubleClicked()
    signal minimizeButtonClicked()
    signal maximizeButtonClicked()
    signal restoreButtonClicked()
    signal closeButtonClicked()

    property var windowVisibility
    property string titleName

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onPressed: {
            mousePressed();
        }

        onDoubleClicked: {
            root.doubleClicked();
        }

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 12
            anchors.rightMargin: 12
            anchors.bottomMargin: 10
            spacing: 15

            Image {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignBottom
                source: "images/logo.png"
            }

            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignBottom
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                color: "white"
                font.pixelSize: 20
                text: titleName
            }

            ImageButton {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignBottom
                imageSource: "images/minimize.png"

                onClicked: {
                    minimizeButtonClicked();
                }
            }

            ImageButton {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignBottom
                imageSource: {
                    if (root.windowVisibility === Window.Maximized) {
                        return "images/maximize_exit.png";
                    } else {
                        return "images/maximize.png";
                    }
                }

                onClicked: {
                    if (root.windowVisibility === Window.Maximized) {
                        restoreButtonClicked();
                    } else {
                        maximizeButtonClicked();
                    }
                }
            }

            ImageButton {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignBottom
                imageSource: "images/close.png"

                onClicked: {
                    closeButtonClicked();
                }
            }
        }
    }
}
