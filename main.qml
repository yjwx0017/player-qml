import QtQuick 2.15
import QtQuick.Window 2.15
import QtMultimedia 5.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.15

Window {
    id: root
    width: 1280
    height: 800
    visible: true
    color: "transparent"
    title: "Player"
    flags: Qt.Window | Qt.FramelessWindowHint
    minimumWidth: 800
    minimumHeight: 500

    property bool isFullScreen: visibility === Window.FullScreen
    property int borderSize: 6

    onIsFullScreenChanged: {
        if (!isFullScreen) {
            hideTimer.stop();
        } else {
            bottomBar._visible = true;
            hideTimer.restart();
        }
    }

    FileDialog {
        id: fileDialog
        title: "打开文件"
        folder: shortcuts.home
        selectFolder: false
        selectExisting: true
        selectMultiple: false
        onAccepted: {
            video.source = fileDialog.fileUrl;
            video.play();
        }
    }

    // Resize border
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onPositionChanged: {
            if (mouse.x < borderSize && mouse.y < borderSize) {
                cursorShape = Qt.SizeFDiagCursor;
            } else if (mouse.x < borderSize && mouse.y > height - borderSize) {
                cursorShape = Qt.SizeBDiagCursor;
            } else if (mouse.x > width - borderSize && mouse.y < borderSize) {
                cursorShape = Qt.SizeBDiagCursor;
            } else if (mouse.x > width - borderSize && mouse.y > height - borderSize) {
                cursorShape = Qt.SizeFDiagCursor;
            } else if (mouse.x < borderSize || mouse.x > width - borderSize) {
                cursorShape = Qt.SizeHorCursor;
            } else if (mouse.y < borderSize || mouse.y > height - borderSize) {
                cursorShape = Qt.SizeVerCursor;
            }
        }
        onPressed: {
            if (mouse.x < borderSize && mouse.y < borderSize) {
                root.startSystemResize(Qt.TopEdge | Qt.LeftEdge);
            } else if (mouse.x < borderSize && mouse.y > height - borderSize) {
                root.startSystemResize(Qt.BottomEdge | Qt.LeftEdge);
            } else if (mouse.x > width - borderSize && mouse.y < borderSize) {
                root.startSystemResize(Qt.TopEdge | Qt.RightEdge);
            } else if (mouse.x > width - borderSize && mouse.y > height - borderSize) {
                root.startSystemResize(Qt.BottomEdge | Qt.RightEdge);
            } else if (mouse.x < borderSize) {
                root.startSystemResize(Qt.LeftEdge);
            } else if (mouse.x > width - borderSize) {
                root.startSystemResize(Qt.RightEdge);
            } else if (mouse.y < borderSize) {
                root.startSystemResize(Qt.TopEdge);
            } else if (mouse.y > height - borderSize) {
                root.startSystemResize(Qt.BottomEdge);
            }
        }
    }

    Rectangle {
        id: mainArea
        anchors.fill: parent
        anchors.margins: (isFullScreen || visibility === Window.Maximized) ? 0 : 6
        color: "#101010"
        clip: true

        // 窗口阴影
        layer.enabled: true
        layer.effect: DropShadow {
            anchors.fill: mainArea
            horizontalOffset: 0
            verticalOffset: 0
            radius: borderSize
            samples: borderSize * 2 + 1
            color: "#66666666"
            spread: 0.0
            source: mainArea
        }

        TitleBar {
            id: titleBar
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: isFullScreen ? 0 : 50
            windowVisibility: root.visibility
            titleName: {
                let fileName = video.source.toString();
                let index = fileName.lastIndexOf("/");
                if (index >= 0) {
                    return fileName.slice(index + 1, fileName.length);
                }

                return fileName;
            }

            onMousePressed: {
                root.startSystemMove();
            }

            onTopEdgeMousePressed: {
                root.startSystemResize(Qt.TopEdge);
            }

            onDoubleClicked: {
                if (root.visibility === Window.Maximized) {
                    root.showNormal();
                } else {
                    root.showMaximized();
                }
            }
            onMinimizeButtonClicked: {
                root.showMinimized();
            }

            onMaximizeButtonClicked: {
                root.showMaximized();
            }

            onRestoreButtonClicked: {
                root.showNormal();
            }

            onCloseButtonClicked: {
                Qt.quit();
            }
        }

        Video {
            id: video
            anchors.top: titleBar.bottom
            anchors.bottom: isFullScreen ? parent.bottom : bottomBar.top
            anchors.left: parent.left
            anchors.right: parent.right
            volume: bottomBar.volume

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onPositionChanged: {
                    if (isFullScreen) {
                        bottomBar._visible = true;
                        hideTimer.restart();
                    }
                }
                onDoubleClicked: {
                    bottomBar.switchFullScreen();
                }
            }

            OpenButton {
                width: 140
                height: 50
                anchors.centerIn: parent
                visible: video.playbackState === MediaPlayer.StoppedState
                enabled: visible

                onClicked: {
                    // 全屏模式下打开文件对话框时，主界面显示异常
                    if (isFullScreen) {
                        root.showMaximized();
                    }

                    fileDialog.open();
                }
            }
        }

        BottomBar {
            id: bottomBar
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 70
            playbackState: video.playbackState
            muted: video.muted
            mediaPosition: video.position
            mediaDuration: video.duration
            isFullScreen: root.isFullScreen
            visible: isFullScreen ? _visible : true

            property var lastVisibility
            property bool _visible: true

            Timer {
                id: hideTimer
                interval: 2000
                onTriggered: {
                    bottomBar._visible = false;
                }
            }

            onMouseEntered: {
                if (isFullScreen) {
                    bottomBar._visible = true;
                    hideTimer.stop();
                }
            }

            onMouseExited: {
                if (isFullScreen) {
                    hideTimer.restart();
                }
            }

            onMousePressed: {
                root.startSystemMove();
            }

            onDoubleClicked: {
                if (root.visibility === Window.Maximized) {
                    root.showNormal();
                } else {
                    root.showMaximized();
                }
            }

            onPlayButtonClicked: {
                video.play();
            }

            onPauseButtonClicked: {
                video.pause();
            }

            onStopButtonClicked: {
                video.stop();
            }

            onVolumeButtonClicked: {
                video.muted = !video.muted;
            }

            onFullScreenButtonClicked: {
                switchFullScreen();
            }

            onSeek: {
                if (video.playbackState !== MediaPlayer.StoppedState) {
                    video.seek(pos);
                }
            }

            function switchFullScreen() {
                if (isFullScreen) {
                    root.visibility = lastVisibility;
                } else {
                    lastVisibility = visibility;
                    root.showFullScreen();
                }
            }
        }
    }
}
