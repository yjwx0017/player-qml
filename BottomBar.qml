import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtMultimedia 5.15

Rectangle {
    id: root
    color: isFullScreen ? "#AA2E2E36" : "#2E2E36"

    signal mousePressed()
    signal doubleClicked()
    signal playButtonClicked()
    signal pauseButtonClicked()
    signal stopButtonClicked()
    signal prevButtonClicked()
    signal nextButtonClicked()
    signal volumeButtonClicked()
    signal fullScreenButtonClicked()
    signal mouseEntered()
    signal mouseExited()
    signal seek(int pos)

    property var playbackState
    property bool muted: false
    property int mediaPosition: 0
    property int mediaDuration: 0
    property bool isFullScreen: false
    property alias volume: volumeBar.value

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            mousePressed();
        }
        onDoubleClicked: {
            root.doubleClicked();
        }
        onEntered: {
            mouseEntered();
        }
        onExited: {
            mouseExited();
        }

        RowLayout {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: playButton.left
            anchors.leftMargin: 25
            anchors.rightMargin: 20
            spacing: 20

            // 进度/时长
            Text {
                color: "white"
                font.pixelSize: 14

                property string durationText: {
                    let secondsTotal = Math.floor(mediaDuration / 1000);
                    let seconds = secondsTotal % 60;
                    let minutes = (secondsTotal - seconds) / 60 % 60;
                    let hours = (secondsTotal - seconds - minutes * 60) / 3600;
                    return hours.toString().padStart(2, '0') + ":" +
                            minutes.toString().padStart(2, '0') + ":" +
                            seconds.toString().padStart(2, '0');
                }

                text: {
                    let secondsTotal = Math.floor(mediaPosition / 1000);
                    let seconds = secondsTotal % 60;
                    let minutes = (secondsTotal - seconds) / 60 % 60;
                    let hours = (secondsTotal - seconds - minutes * 60) / 3600;
                    let positionText = hours.toString().padStart(2, '0') + ":" +
                        minutes.toString().padStart(2, '0') + ":" +
                        seconds.toString().padStart(2, '0');
                    return positionText + " / " + durationText;
                }
            }

            Item {
                Layout.fillWidth: true
            }

            // 停止按钮
            ImageButton {
                width: 36
                height: 36
                imageSource: "images/stop.png"

                onClicked: {
                    stopButtonClicked();
                }
            }

            // 上一个
            ImageButton {
                width: 36
                height: 36
                imageSource: "images/previous.png"

                onClicked: {
                    prevButtonClicked();
                }
            }
        }

        // 播放按钮
        ImageButton {
            id: playButton
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: 48
            height: 48
            imageSource: {
                if (playbackState === MediaPlayer.PlayingState) {
                    return "images/pause.png";
                } else {
                    return "images/play.png";
                }
            }

            onClicked: {
                if (playbackState === MediaPlayer.PlayingState) {
                    pauseButtonClicked();
                } else {
                    playButtonClicked();
                }
            }
        }

        RowLayout {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: playButton.right
            anchors.right: parent.right
            anchors.leftMargin: 20
            anchors.rightMargin: 25
            spacing: 20

            // 下一个
            ImageButton {
                width: 36
                height: 36
                imageSource: "images/next.png"

                onClicked: {
                    nextButtonClicked();
                }
            }

            // 音量按钮
            ImageButton {
                width: 24
                height: 24
                imageSource: muted ? "images/volume_mute.png" : "images/volume.png"

                onClicked: {
                    volumeButtonClicked();
                }
            }

            // 音量条
            ProgressBar {
                id: volumeBar
                width: 75
                height: 4
                value: 1

                background: Rectangle {
                    color: "#565656"
                    radius: 3
                }

                contentItem: Item {
                    Rectangle {
                        width: volumeBar.visualPosition * parent.width
                        height: parent.height
                        radius: 2
                        color: "#009bf2"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    onPressed: {
                        volumeBar.value = mouse.x / volumeBar.width;
                    }
                    onPositionChanged: {
                        if (pressed) {
                            volumeBar.value = mouse.x / volumeBar.width;
                        }
                    }
                }
            }

            Item {
                Layout.fillWidth: true
            }

            ImageButton {
                width: 24
                height: 24
                imageSource: {
                    if (visibility === Window.FullScreen) {
                        return "images/fullscreen_exit.png";
                    } else {
                        return "images/fullscreen.png";
                    }
                }

                onClicked: {
                    fullScreenButtonClicked();
                }
            }
        }

        // 进度条
        ProgressBar {
            id: progressBar
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 4
            value: mediaPosition / mediaDuration

            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 6
                color: "#565656"
                radius: 3
            }

            contentItem: Item {
                implicitWidth: 200
                implicitHeight: 4

                Rectangle {
                    width: progressBar.visualPosition * parent.width
                    height: parent.height
                    radius: 2
                    color: "#009bf2"
                }
            }

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onPressed: {
                    seek(mouse.x / progressBar.width * mediaDuration);
                }
                onPositionChanged: {
                    if (pressed) {
                        seek(mouse.x / progressBar.width * mediaDuration);
                    }
                }
            }
        }
    }
}
