import QtQuick 2.15

MouseArea {
    id: root
    hoverEnabled: true
    cursorShape: Qt.PointingHandCursor

    property alias imageSource: image.source

    Image {
        id: image
        anchors.fill: parent
        opacity: root.containsMouse ? 1 : 0.8
    }
}
