import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Button {
    id: openButton
    opacity: hovered ? 1 : 0.8

    contentItem: Item {}

    background: Rectangle {
        color: "#009BF2"
        radius: 8

        RowLayout {
            anchors.fill: parent
            spacing: 8

            Item {
                Layout.fillWidth: true
            }

            Text {
                Layout.bottomMargin: 4
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                text: "打开文件"
                font.pixelSize: 16
                color: "white"
            }

            Image {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                source: "images/open.png"
            }

            Item {
                Layout.fillWidth: true
            }

        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed:  mouse.accepted = false
        cursorShape: Qt.PointingHandCursor
    }
}
